import 'package:alpha/app/modules/widgets/action_toolbar.dart';
import 'package:flutter/material.dart';
import 'package:alpha/app/modules/widgets/slide_button_switch.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../widgets/step_progress_indikator.dart';
import '../controllers/discover_controller.dart';
import 'package:alpha/app/modules/widgets/background.dart';
import 'package:alpha/app/modules/widgets/btn_toggle_pm.dart';

class DiscoverView extends GetView<DiscoverController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Background(img: 'assets/images/cover4.png'),
          Positioned(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 120),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ActionsToolbar(),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35, left: 15),
            child: Container(
              height: 35.0,
              width: 95.0,
              child: Column(
                children: [
                  SizedBox(height: 2),
                  Text(
                    "My Balance:",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                  Text(
                    "155,4257 ACO",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                  // 155,4257 ACO
                ],
              ),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(30.0)),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 180, left: 15),
                child: Container(
                  height: 30.0,
                  width: 100.0,
                  child: Center(
                    child: Text(
                      "ENDING SOON",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 12, color: HexColor("#FFC040")),
                    ),
                  ),
                  decoration: BoxDecoration(
                      color: HexColor('#FFC040').withOpacity(0.5),
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 270, left: 15),
                child: Container(
                  child: Center(
                    child: Text(
                      "This is the project name",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 70, left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  BtnTogglePM(),
                ],
              ),
            ),
          ),
          Positioned(
            child: Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SlideButtonSwitch(title: "SEND 1000,00 ACO"),
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 125,
              left: 20,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  StepProgressIndikator(),
                ],
              )),
        ],
      ),
    );
  }
}

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  //TODO: Implement LoginController
  TextEditingController emailC = TextEditingController();
  TextEditingController passC = TextEditingController();

  RxBool isHidden = true.obs;

  void login() async {
    if (emailC.text == "admin@gmail.com" && passC.text == "admin") {
      Get.offAllNamed("/dashboard");
    } else {
      Get.defaultDialog(
        title: "Terjadi kesalahan",
        middleText: "Email & Password tidak sesuai",
      );
    }
  }

  void logout() {
    Get.offAllNamed("/login");
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../widgets/background.dart';
import '../controllers/explore_controller.dart';

class ExploreView extends GetView<ExploreController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Background(img: 'assets/images/cover4.png'),
          Container(
            child: Center(
              child: Text(
                'Explore waiting instruction',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  backgroundColor: Colors.amber[400],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

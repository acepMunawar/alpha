import 'package:alpha/app/modules/discover/views/discover_view.dart';
import 'package:alpha/app/modules/echo/views/echo_view.dart';
import 'package:alpha/app/modules/explore/views/explore_view.dart';
import 'package:alpha/app/modules/more/views/more_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import '../controllers/dashboard_controller.dart';
import '../../home/views/home_view.dart';

class DashboardView extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(builder: (controller) {
      return DefaultTabController(
        initialIndex: 0,
        length: 5,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          extendBody: true,
          body: Container(
            child: TabBarView(
              children: [
                DiscoverView(),
                ExploreView(),
                HomeView(),
                EchoView(),
                MoreView(),
              ],
            ),
          ),
          bottomNavigationBar: TabBar(
              labelStyle: TextStyle(
                fontSize: 8.0,
              ),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.label,
              labelPadding: const EdgeInsets.only(bottom: 1.0),
              indicatorPadding: EdgeInsets.all(5.0),
              indicatorColor: Colors.white,
              tabs: [
                Tab(
                  icon: Icon(CupertinoIcons.compass),
                  text: 'DISCOVER',
                ),
                Tab(
                  icon: Icon(CupertinoIcons.search),
                  text: 'EXPLORE',
                ),
                Tab(
                  icon: Icon(Icons.dashboard),
                  text: 'DASH-BOARD',
                ),
                Tab(
                  icon: Icon(CupertinoIcons.bubble_left),
                  text: 'ECHO',
                ),
                Tab(
                  icon: Icon(CupertinoIcons.ellipsis),
                  text: 'MORE',
                ),
              ]),
        ),
      );
    });
  }
}

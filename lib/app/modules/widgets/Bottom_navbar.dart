import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
class Bottom_navbar extends StatelessWidget {

  int tabIndex = 0;

  get tabController => null;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GFAppBar(
        title: const Text('UI Kit'),
      ),
      body: GFTabBarView(controller: tabController, children: <Widget>[
        Container(color: Colors.red),
        Container(color: Colors.green),
        Container(color: Colors.blue)
      ]),
      bottomNavigationBar: GFTabBar(
       length: 3,
        controller: tabController,
        tabs: [
          Tab(
            icon: Icon(Icons.directions_bike),
            child: const Text(
              'Tab1',
            ),
          ),
          Tab(
            icon: Icon(Icons.directions_bus),
            child: const Text(
              'Tab2',
            ),
          ),
          Tab(
            icon: Icon(Icons.directions_railway),
            child: const Text(
              'Tab3',
            ),
          ),
        ],
      ),
    );




  }




}

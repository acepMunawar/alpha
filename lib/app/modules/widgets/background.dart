import 'package:flutter/material.dart';

Widget Background({String? img}) {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
          colorFilter:
              ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.darken),
          image: AssetImage(img!),
          fit: BoxFit.cover),
    ),
  );
}

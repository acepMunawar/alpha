import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:toggle_switch/toggle_switch.dart';

Widget BtnTogglePM({int? value}) {
  return ToggleSwitch(
    minWidth: 30.0,
    minHeight: 25.0,
    cornerRadius: 50.0,
    activeBgColors: [
      [HexColor('#F7F6F8')],
      [HexColor('#F7F6F8')]
    ],
    borderWidth: 5.0,
    borderColor: [Colors.white],
    activeFgColor: HexColor('#F36868'),
    inactiveBgColor: Colors.white,
    inactiveFgColor: Colors.black,
    initialLabelIndex: 1,
    totalSwitches: 2,
    labels: [
      '-',
      '+',
    ],
    radiusStyle: true,
    fontSize: 17,
    onToggle: (value) {
      print('switched to: $value');
      print('tes2');
    },
  );
}

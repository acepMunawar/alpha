import 'package:flutter/material.dart';
import 'package:action_slider/action_slider.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget SlideButtonSwitch({ActionSliderController? controller, String? title}) {
  final String assetName = 'assets/icons/card_arrow.svg';

  return Stack(children: [
    Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 15, bottom: 70),
          child: ActionSlider.standard(
            rolling: true,
            width: 240.0,
            height: 40.0,
            // controller: _controller,
            child: Text(title!, style: TextStyle(color: Colors.white)),
            backgroundColor: Colors.black.withOpacity(0.5),
            reverseSlideAnimationCurve: Curves.easeInOut,
            reverseSlideAnimationDuration: const Duration(milliseconds: 500),
            toggleColor: HexColor("#F36868"),
            icon: SvgPicture.asset(
              assetName,
              color: Colors.white,
              semanticsLabel: 'Label',
              width: 10,
              height: 10,
            ),
            // borderWidth: 10,
            onSlide: (controller) async {
              // controller.loading(); //starts loading animation
              // await Future.delayed(const Duration(seconds: 3));
              controller.success(); //starts success animation
              await Future.delayed(const Duration(seconds: 1));
              controller.reset();
              //resets the slider
            },
          ),
        ),
      ],
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 15, bottom: 70),
          child: ActionSlider.standard(
            rolling: true,
            width: 40.0,
            height: 40.0,
            backgroundColor: Colors.transparent,
            reverseSlideAnimationCurve: Curves.easeInOut,
            reverseSlideAnimationDuration: const Duration(milliseconds: 500),
            toggleColor: HexColor("#F36868").withOpacity(0.5),
            icon: Icon(
              Icons.more_horiz,
              color: Colors.transparent,
            ),
          ),
        ),
      ],
    ),
  ]);
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:get/get.dart';

class ActionsToolbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70.0,
      child: Column(children: [
        SizedBox(height: 20),
        _getFollowAction(),
        _getLikeButton(
          iconFirst: Icons.emoji_events_outlined,
          iconSecond: Icons.emoji_events,
          leftValue: 3.0,
          bottomValue: 2.0,
          topValue: 0.0,
          title: '#2',
          colorValueFirst: '#ffffff',
          colorValueSecond: '#ffca00',
          leftValueText: 13.0,
        ),
        _getLikeButton(
          iconFirst: CupertinoIcons.heart,
          iconSecond: CupertinoIcons.heart_fill,
          title: '21k',
          leftValue: 3.0,
          bottomValue: 0.0,
          topValue: 3.0,
          colorValueFirst: '#ffffff',
          colorValueSecond: '#ff0000',
          leftValueText: 10.0,
        ),
        _getShareAction(
            icon: CupertinoIcons.arrowshape_turn_up_right, title: '12k'),
      ]),
    );
  }

  Widget _getShareAction(
      {String? title, IconData? icon, bool isShare = false}) {
    return Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Stack(children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            // color: Colors.amber,
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.1),
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: IconButton(
              onPressed: () {
                print("say hello");
                Get.dialog(
                  Container(
                    color: HexColor("2F284E").withOpacity(0.5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              TextButton(
                                child: Text(
                                  'X',
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.white),
                                ),
                                onPressed: () {
                                  Get.back();
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  useSafeArea: true,
                );
              },
              icon: Padding(
                padding: const EdgeInsets.only(left: 3),
                child: Icon(
                  icon,
                  size: 20,
                  color: Colors.grey[300],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 47, left: 10),
            child: Text(
              title!,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: isShare ? 10.0 : 12.0,
                  fontFamily: 'TitilliumWeb-litle'),
            ),
          )
        ]));
  }

  Widget _getPlusIcon() {
    return Positioned(
      top: 33,
      left: ((50.0 / 2) - (20.0 / 2)),
      child: Container(
          width: 12.0,
          height: 12.0,
          decoration: BoxDecoration(
              color: Color.fromARGB(255, 255, 43, 84),
              borderRadius: BorderRadius.circular(5.0)),
          child: Icon(
            Icons.add,
            color: Colors.white,
            size: 10,
          )),
    );
  }

  Widget _getProfilePicture() {
    return Container(
      width: 40.0,
      height: 39.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          border: Border.all(width: 3, color: HexColor("#F36868"))),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15.0)),
        child: Image.asset('assets/images/st1.png'),
      ),
    );
  }

  Widget _getFollowAction({String? pictureUrl}) {
    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 10.0),
          width: 40.0,
          height: 50.0,
          child: Stack(children: [_getProfilePicture(), _getPlusIcon()])),
    );
  }

  Widget _getLikeButton(
      {String? title,
      IconData? iconFirst,
      IconData? iconSecond,
      String? colorValueSecond,
      String? colorValueFirst,
      double? leftValue,
      double? bottomValue,
      double? topValue,
      double? leftValueText,
      bool isShare = false}) {
    String ColorValueSecond = colorValueSecond!;
    String ColorValueFirst = colorValueFirst!;
    double LeftValue = leftValue!;
    double BottomValue = bottomValue!;
    double TopValue = topValue!;
    double LeftValueText = leftValueText!;
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Stack(children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 10.0),
          width: 40.0,
          height: 40.0,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.1),
            borderRadius: BorderRadius.circular(50.0),
          ),
          child: Padding(
            padding: EdgeInsets.only(
                left: LeftValue, bottom: BottomValue, top: TopValue),
            child: LikeButton(
              likeBuilder: (isLiked) {
                if (isLiked)
                  return Icon(iconSecond,
                      size: 20, color: HexColor(ColorValueSecond));
                if (!isLiked)
                  return Icon(iconFirst,
                      size: 20, color: HexColor(ColorValueFirst));
              },
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 47, left: LeftValueText),
          child: Text(
            title!,
            style: TextStyle(
              color: Colors.white,
              fontSize: isShare ? 10.0 : 12.0,
              fontFamily: 'TitilliumWeb-litle',
            ),
          ),
        ),
      ]),
    );
  }
}

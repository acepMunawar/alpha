import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'dart:math' as math;

class StepProgressIndikator extends StatelessWidget {
  const StepProgressIndikator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Stack(
          children: [
            CircularStepProgressIndicator(
              totalSteps: 20,
              // step
              currentStep: 5,
              padding: math.pi / 15,
              selectedColor: HexColor("#FFC040"),
              unselectedColor: HexColor("#FAFAFD").withOpacity(0.1),
              selectedStepSize: 9.0,
              unselectedStepSize: 9.0,
              width: 80,
              height: 80,
            ),
            Positioned(
              left: 17,
              top: 17,
              child: CircularStepProgressIndicator(
                totalSteps: 20,
                // step
                currentStep: 10,
                padding: math.pi / 15,
                selectedColor: HexColor("#00DAA1"),
                unselectedColor: HexColor("#FAFAFD").withOpacity(0.1),
                selectedStepSize: 9.0,
                unselectedStepSize: 9.0,
                width: 45,
                height: 45,
              ),
            ),
            Positioned(
              left: 30,
              top: 29,
              child: Text(
                "47%",
                style: TextStyle(fontSize: 12, color: HexColor("#00DAA1")),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    color: Colors.amber,
                    width: 8,
                    height: 8,
                    // child: ,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Remaining 1d 15h 23",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                children: [
                  Container(
                    color: Colors.amber,
                    width: 8,
                    height: 8,
                    // child: ,
                  ),
                  SizedBox(width: 10),
                  Text(
                    " 7.784,00 ACO raised ",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

import 'package:alpha/app/modules/widgets/background.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import '../controllers/home_controller.dart';
import 'package:action_slider/action_slider.dart';
import 'package:toggle_switch/toggle_switch.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Background(img: 'assets/images/cover4.png'),
          Container(
            child: Center(
              child: Text(
                'Dashboard waiting instruction',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  backgroundColor: Colors.amber[400],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

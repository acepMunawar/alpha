import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/echo_controller.dart';
import '../../widgets/background.dart';

class EchoView extends GetView<EchoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Background(img: 'assets/images/cover4.png'),
          Container(
            child: Center(
              child: Text(
                'Echo waiting instruction',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  backgroundColor: Colors.amber[400],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:get/get.dart';

import '../controllers/echo_controller.dart';

class EchoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EchoController>(
      () => EchoController(),
    );
  }
}

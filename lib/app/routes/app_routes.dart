part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const DISCOVER = _Paths.DISCOVER;
  static const EXPLORE = _Paths.EXPLORE;
  static const ECHO = _Paths.ECHO;
  static const MORE = _Paths.MORE;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const DASHBOARD = '/dashboard';
  static const DISCOVER = '/discover';
  static const EXPLORE = '/explore';
  static const ECHO = '/echo';
  static const MORE = '/more';
}
